# HTTP Services

Biblioteca PHP para disponibilização ou consumo de APIs REST 

## Instalação

A instalação pode ser feita utilizando composer: 

```bash
$ composer require rafaelrsilva/http-services
```

---

### Requerimentos

 - Esta biblioteca utiliza funções anônimas e namespaces, por isso necessita de uma versão PHP >= 5.3.2
 - Dependendo do modo como as funções anônimas forem utilizadas, pode ser necessário uma versão do PHP ainda mais recente. Saiba mais sobre isso no [Manual do PHP](http://php.net/manual/pt_BR/functions.anonymous.php).

---

## Utilização

Esta biblioteca possui 2 modos de utilização:

 - Criação de API
 - Consumo de API

---

## Exemplo de Criação de API

```php
<?php require_once 'vendor/autoload.php';

use rafaelrsilva\httpservices\SimpleRestApi;

$api = new SimpleRestApi();

$api->get('categorias', function($request, $response){
    //...
});

$api->get('categorias/(:num)/produtos', function($request, $response){
    //...
});

$api->get('produtos/(:num)', function($request, $response){
    //...
});

$api->post('produtos', function($request, $response){
    //...
});
?>
```

### Métodos da API

#### $api->get($path, $response)

Cria um listener GET para o caminho de recurso especificado.

Parâmetros:

- `$path`: Caminho do recurso.
- `$response`: Callback que deve ser chamado para retorar os dados para o solicitante.

Retorno: 

- Nenhum retorno

---

#### $api->post($path, $response)

Cria um listener POST para o caminho de recurso especificado.

Parâmetros:

- `$path`: Caminho do recurso.
- `$response`: Callback que deve ser chamado para retorar os dados para o solicitante.

Retorno: 

- Nenhum retorno

---

#### $api->start()

Método utilizado para iniciar a API.

Parâmetros:

- Nenhum parâmetro.

Retorno: 

- Nenhum retorno.

Obs.: Este método deve ser chamado após toda a configuração da API.

---

#### Variável $path: Parâmetros no meio da url (URL parameter)

Para colocar parâmetros no meio da URL, pode-se utilizar os seguintes wildcards:

- `(:num)`: Mapeia qualquer caracter numérico. Ex.: categorias/(:num)/produtos => categorias/1/produtos
- `(:any)`: Mapeia qualquer caracter alfa-numérico antes da proxima barra. Ex.: noticias/(:any) => noticias/titulo-da-noticia
- `(:all)`: Mapeia qualquer caracter alfa-numérico a partir do wildcard.: Ex.: noticias/(:all) => noticias/titulo-da-noticia/outro-slug/...


---


#### Variável $request

Array associativo contendo as seguintes chaves:

- `header`: Parâmetros do header da requisição;
- `query`: Parâmetros enviados no final da URL (Query String);
- `body`: Parâmetros enviados pelo body da requisição;
- `uri`: Array contento a URL da requisição.

OBS.: Nesta versão da biblioteca, os parâmetros enviados no meio da URL não são separados da URL automaticamente.
Neste caso, os parâmetros devem ser acessados manualmente atrevés da variável $request['uri'].


Se o path for 'categorias/(:num)/produtos' e for chamado 'categorias/12/produtos', o array da 'uri' será:

```php
<?php
array('categorias', '12', 'produtos');
?>
```

Daí é só acessar através do índice do array:

```php
<?php
$categoriaID = $request['uri'][1];
?>
```




---

#### Variável $response

Função anônima utilizada para retornar os dados para quem efetuou a requisição. Possui os seguintes parâmetros

- `responseData`: Dados a serem retornado. Ex.: '{}';
- `contentType`: Tipo dos dados a serem retornado. Ex.: 'application/json';
- `code`: Código HTTP da resposta. Ex.: '200 OK',


---

## Exemplo Consumo de API

```php
<?php require_once 'vendor/autoload.php';

use rafaelrsilva\httpservices\HttpService;

//Criação do Service
class MyService extends HTTPService {
	
	//Endpoint das requisições
	protected function getEndpoint(){
		return 'http://api.example.com/v1';
	}

	//Header padrão que será enviado em todas as requisições
	protected function getDefaultHeader(){
		return array(
			'Content-Type'  => 'application/json; charset=UTF-8',
			'Authorization' => 'Basic dXN1YXJpbzpzZW5oYQ==',
		);
	}
	
	public function getCategorias(){

		//Envia uma requisição GET para 'http://api.example.com/v1/categorias' sem passar parâmetros
		return $this->doGetRequest('categorias', array());
	}

	public function getProdutos($categoriaID){
		return $this->doGetRequest('categorias/'.$categoriaID.'/produtos', array());
	}

	public function getProduto($produtoID){
		return $this->doGetRequest('produtos/'.$produtoID, array());
	}

	public function setProduto($nome, $descricao){

		//Envia uma requisição POST para 'http://api.example.com/v1/produtos' passando 2 parâmetros no body
		return $this->doPostRequest('produtos', array('nome' => $nome, 'descricao' => $descricao));
	}
}


//Utilização do Service

$myService = new MyService();
$myService.setProduto('Nome', 'Descrição');

?>

```
### Métodos do Service


#### $service->doRequest($path, $method, $parametrer = array(), $customHeader = array())

Envia uma requisição para o endpoint do service.


Parâmetros:

- `$path`: Caminho do recurso solicitado.
- `$method`: Verbo HTTP da requisição ('GET', 'POST', 'PUT', 'DELETE').
- `$parametros`: Array contendo os parâmetros a serem enviados na requisição.
- `$customHeader`: Array contendo os parâmetros a serem enviados no header, além do default.

Retorno: 

- Retorno do endpoint do service.

---

#### $service->doGetRequest($path, $parametrer = array(), $customHeader = array())

Envia uma requisição GET para o endpoint do service.

Parâmetros:

- `$path`: Caminho do recurso solicitado.
- `$parametros`: Array contendo os parâmetros a serem enviados na URL da requisição.
- `$customHeader`: Array contendo os parâmetros a serem enviados no header, além do default.

Retorno: 

- Retorno do endpoint do service.

---

#### $service->doPostRequest($path, $parametrer = array(), $customHeader = array())

Envia uma requisição POST para o endpoint do service.

Parâmetros:

- `$path`: Caminho do recurso solicitado.
- `$parametros`: Array contendo os parâmetros a serem enviados na body da requisição.
- `$customHeader`: Array contendo os parâmetros a serem enviados no header, além do default.

Retorno: 

- Retorno do endpoint do service.

---
#### $service->returnResponse($data, $error = array(), $header = array())

Padroniza os dados de retorno das requisições.
Os métodos doRequest, doGetRequest e doPostRequest utilizam este método.


Parâmetros:

- `$data`: Dados retornados da requisiçào.
- `$error`: Array contendo os informações de erros, caso tenha ocorrido.
- `$header`: Array contendo o header de resposta.

Retorno: 

- Array associativo contendo os 3 parametros passados para o método.

Obs.: Utilize este método ao utilizar mock do endpoint para que o formato de retorno seja igual ao esperado do endpoint.