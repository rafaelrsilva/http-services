<?php

namespace rafaelrsilva\httpservices;

/**
 * HTTPService
 * Classe abstrata para implementação de serviço para consumo de API REST
 *
 * @author Rafael Ribeiro da Silva
 */
abstract class HttpService {
	abstract protected function getEndpoint();
	abstract protected function getDefaultHeader();

	protected $errors = array();

	protected function customErrorHandler($errno, $errstr){
		$this->errors[] = array('errno' => $errno, 'errstr' => $errstr); 
	}

	protected function returnResponse($data, $error = array(), $header = array()){
		return array('error' => $error, 'header' => $header, 'data' => $data);
	}

	protected function doRequest($path, $method, array $parameters = array(), array $customHeader = array()){
		$header = array_merge($this->getDefaultHeader(), $customHeader);

		foreach($header as $key => &$value){
			$value = $key.': '.$value;
		}

		$streamContext = stream_context_create(array('http' => array(
			'method'  => $method,
			'header'  => sprintf("%s\r\n", implode("\r\n", $header)),
			'content' => http_build_query($parameters),
		)));

		$this->errors = array();

		set_error_handler(array($this, 'customErrorHandler'));

		$result = json_decode(file_get_contents(trim($this->getEndpoint(), '/').'/'.trim($path, '/'), false, $streamContext), true);

		restore_error_handler();

		return $this->returnResponse(((!$result) ? array() : $result), $this->errors, ((!@$http_response_header) ? array() : $http_response_header));
	}

	protected function doGetRequest($path, array $parameters = array(), array $customHeader = array()){
		return $this->doRequest($path, 'GET', $parameters, $customHeader);
	}

	protected function doPostRequest($path, array $parameters = array(), array $customHeader = array()){
		return $this->doRequest($path, 'POST', $parameters, $customHeader);
	}
} 

?>