<?php

namespace rafaelrsilva\httpservices;

/**
 * SimpleRestApi
 * Classe para implementação simplificada de API REST
 *
 * @author Rafael Ribeiro da Silva
 */

class SimpleRestApi {

	private $script_name;
	private $request_uri;
	private $request_method;
	private $query_parameters;
	private $body_parameters;

	private $routes;

	private $middleware;

	protected function getRegex($str){
		$str = str_replace('/', '\/', $str);
		$str = str_replace('(:all)', '(:any)(\/([a-zA-Z0-9\-\+])*)*', $str);
		$str = str_replace('(:any)', '([a-zA-Z0-9\-\+])*', $str);
		$str = str_replace('(:num)', '([0-9])*', $str);
		return "/^".$str."$/";
	}

	function __construct($middleware = null){
		$this->script_name = explode('/', $_SERVER['SCRIPT_FILENAME']);
		$this->script_name = array_pop($this->script_name);

		$this->request_uri = explode($this->script_name, $_SERVER['REQUEST_URI']);
		$this->request_uri = array_pop($this->request_uri);
		$this->request_uri = explode(((strstr($this->request_uri, '/?')) ? '/?' : '?'), $this->request_uri);
		$this->request_uri = array_shift($this->request_uri);
	
		$this->request_uri = trim($this->request_uri, '/');

		$this->request_method = $_SERVER['REQUEST_METHOD'];

		$this->query_parameters = $_GET;
		$this->body_parameters = $_POST;

		$this->routes = array();

		$this->middleware = $middleware;
	}

	public function get($path, $callback){
		$this->routes[] = array(
			'path'     => $this->getRegex($path),
			'method'   => 'GET',
			'callback' => $callback
		);
	}

	public function post($path, $callback){
		$this->routes[] = array(
			'path'     => $this->getRegex($path),
			'method'   => 'POST',
			'callback' => $callback
		);
	}

	public function start(){
		$request = array(
			'header' => getallheaders(),
			'query'  => $this->query_parameters,
			'body'   => $this->body_parameters,
			'uri'    => explode('/', $this->request_uri)
		);

		$response = function($responseData = '', $contentType = false, $code = false){
			if($contentType){
				header('Content-Type: '.$contentType);
			}

			if($code){
				header('HTTP/1.1 '.$code);
			}

			echo $responseData;
			exit;
		};

		if(($middleware = $this->middleware)){
			$middleware($request, $response);
		}

		foreach($this->routes as $route){
			if(preg_match($route['path'], $this->request_uri) && $route['method'] == $this->request_method){
				header('HTTP/1.1 200 OK');

				$route['callback']($request, $response);
				exit;
			}
		}

		header('HTTP/1.1 404 Not Found');
	}
}

?>